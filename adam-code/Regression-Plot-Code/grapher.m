T = table2array(readtable('Covariate-Coefficient-Data/LRcoeffsRAT.csv'));

T(:,1) = []

T2 = transpose(T)

deckMax = 14

x = (1:21) + 2002
hold on

%CumulSum is cumulative sum of freqMat
cumulSum = transpose(abs(T))
%Generate random color for each deck
colors = rand(deckMax, 3);
%Put correct values in cumulSum
for j = 1:deckMax
    if j ~= deckMax
        cumulSum(j+1, :) = cumulSum(j+1, :) + cumulSum(j, :);
    end
end

for i = 1:21
    cumulSum(:,i) = cumulSum(:,i) / cumulSum(deckMax, i)
end

for j = 1:deckMax
    plot(x,cumulSum(j, :), 'color', colors(j, :), 'LineWidth', 8);
end

%Set colors for the area
colororder(colors)
%Plot area under graph in reverse order
for j = 1:deckMax
    k = (deckMax + 1) - j;
    area(x,cumulSum(k,:))
end

title('Linear Regression Coefficient Proportions by Season')
xtickangle(45)

legend({'RATScore', 'RATFGM', 'RATFG', 'RATFGM3', 'RATFGA3', 'RATFTM', 'RATFTA', 'RATOR', 'RATDR', 'RATAst', 'RATTO', 'RATStl', 'RATBlk', 'RATPF'}, 'Location', 'northwest')
