
names = ["Covariate-Coefficient-Data-Mod2/LRcoeffsRAT.csv" 
    "Covariate-Coefficient-Data-Mod2/LASSOcoeffsRAT.csv" 
    "Covariate-Coefficient-Data-Mod2/RIDGEcoeffsRAT.csv" 
    "Covariate-Coefficient-Data-Mod2/ELcoeffsRAT.csv"];

graphnames = ["Linear Regression" "LASSO" "RIDGE" "Elastic Net"];
deckMax = 5;

rng(6);

%Generate random color for each deck
colors = rand(deckMax, 3);

%Set colors for the area
colororder(colors)

for n = 1:4

    T = table2array(readtable(names(n)));
    
    subplot(2,2,n)
    
    T(:,1) = [];
    
    T2 = transpose(T);

    x = (1:21) + 2002;
    hold on
    
    %CumulSum is cumulative sum of freqMat
    cumulSum = transpose(abs(T));
    %Put correct values in cumulSum
    for j = 1:deckMax
        if j ~= deckMax
            cumulSum(j+1, :) = cumulSum(j+1, :) + cumulSum(j, :);
        end
    end
    
    for i = 1:21
        cumulSum(:,i) = cumulSum(:,i) / cumulSum(deckMax, i)
    end
    
    for j = 1:deckMax
        plot(x,cumulSum(j, :), 'color', colors(j, :), 'LineWidth', 8);
    end
    
    %Plot area under graph in reverse order
    for j = 1:deckMax
        k = (deckMax + 1) - j;
        area(x,cumulSum(k,:), 'FaceColor', colors(j, :))
    end

    title(graphnames(n))
    xtickangle(45)
    xlim([2003, 2023])
end

sgtitle('Regression Coefficient Proportions by Season') 
legend({'RATTO', 'RATDR', 'RATFTA','RATFGA', 'RATScore'}, 'Location', 'northwest')
%legend({'RATScore', 'RATFGM', 'RATFGA', 'RATFGM3', 'RATFGA3', 'RATFTM', 'RATFTA', 'RATOR', 'RATDR', 'RATAst', 'RATTO', 'RATStl', 'RATBlk', 'RATPF'}, 'Location', 'northwest')
