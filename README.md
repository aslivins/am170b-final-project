# Exploring the Dynamics of College Basketball

Project with Joseph Halloran and Henry Waterhouse (UCSC)

## Abstract

We explore the most important statistics in making a winning basketball team and how these change over
time. Our data set contains college basketball game data which we clean and modify into a data frame
of team season box score statistics data. PCA and clustering methods reveal what features seperates
teams of similar or differing performance. PCA shows that team performance is centrally distributed,
so we perform outlier detection to identify points of high variation. To see how the importance of box
score statistics change over time, we perform season by season regression on various subsets of features
to track feature coefficient values per season. The visualizations of these feature coefficient values per
season reveal that the statistics most important to winning games have remained relatively constant over
the last 21 years.
